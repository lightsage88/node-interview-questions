class App extends React.Component {
    constructor(props) {
     super(props);
      this.state={
        focusedUser: null
      };
      this.selectUserForInfoBox = this.selectUserForInfoBox.bind(this);
    }

    componentDidMount(){
      this.fetchUsers();
    }
    

    fetchUsers(){
      fetch(`/users/userlist`)
      .then(response => response.json())
      .then(data => {
          this.setState({
              users: data
          })
      })
      .catch(err => console.error(err));
    }

    

      selectUserForInfoBox(user){
        this.setState({
          focusedUser: user
        })

      }

      deleteUser(id){
        var confirmation = confirm("You really want to delete this one?");
        if(confirmation === true) {
          fetch(`/users/deleteUser/${id}`, {
            method: "DELETE",
            headers: {
              "content-type": "application/json"
            }
          })
          .then(()=>{
            this.fetchUsers()
          })
          .catch(err => console.error(err));
        }
      }


    render() {
      let userRows;
      let focusedUser = this.state.focusedUser;
        
      userRows = this.state.users ? this.state.users.map((user)=>{
          return <tr key={user._id}>
                  <td><a href="#" onClick={()=>{this.selectUserForInfoBox(user)}}>{user.username}</a></td>
                  <td>{user.email}</td>
                  <td><a href="#" onClick={()=>{this.deleteUser(user._id)}}>delete</a></td>
                 </tr>
        })
        :
        null;

      
   return (
      <div id="reactVersion"> 
       <h1>The React Version</h1>
       <div id="componentWrapper">
        <UserInfo focusedUser = {focusedUser}/>
        <section id="listAddBox">
        <UserList userRows={userRows} />
        <AddUser fetchUsers={()=>this.fetchUsers()} />
        </section>
        </div>
      </div>
     )
    }
   }
   
   ReactDOM.render(<App/>, document.getElementById('root'));