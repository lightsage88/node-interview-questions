
class AddUser extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        };
        this.adjustInputValue = this.adjustInputValue.bind(this);
        this.submitUser = this.submitUser.bind(this);

    }

    adjustInputValue(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    submitUser(e) {
        e.preventDefault();
        let newUser = this.state;
        
        if(Object.keys(newUser).length < 6 
        || newUser.username == null
         || newUser.email == null 
         || newUser.fullname == null 
         || newUser.gender == null
         || newUser.location == null
         || newUser.age == null
        ) {
            alert('Please fill in all the fields');
        } else {

        fetch('/users/addUser', {
            method:"POST",
            headers: {
                "content-type": "application/json"
            },
            body:JSON.stringify( {
                username: newUser.username || '',
                email: newUser.email || '',
                fullname: newUser.fullname || '',
                age: newUser.age || '',
                location: newUser.location || '',
                gender: newUser.gender || ''
            })
        })
        .then(this.props.fetchUsers())
        .then(()=>{
            this.usernameInput.value = '';
            this.emailInput.value = '';
            this.fullnameInput.value = '';
            this.ageInput.value = '';
            this.locationInput.value = '';
            this.genderInput.value = '';

            this.setState({
                username: null,
                email: null,
                fullname: null,
                age: null,
                location: null,
                gender: null

            })
            
        })
       
        .catch(err => console.error(err));

    }

        
    }

    render(){
        return(
            <div id="addUser-React">
                <h2>Add User</h2>
                <fieldset>
                    <input ref={input => this.usernameInput = input}  id="inputUserName-React" type="text" name="username" placeholder="Username"  onChange={this.adjustInputValue}/>
                    <input ref={input => this.emailInput = input} id="inputUserEmail-React" type="text" name="email" placeholder="Email"  onChange={this.adjustInputValue}/>
                    <br/>
                    <input ref={input => this.fullnameInput = input} id="inputUserFullname-React" type="text" name="fullname" placeholder="Full Name"  onChange={this.adjustInputValue}/>
                    <input  ref={input => this.ageInput = input} id="inputUserAge-React" type="text" name="age" placeholder="Age"  onChange={this.adjustInputValue}/>
                    <br/>
                    <input ref={input => this.locationInput = input} id="inputUserLocation-React" type="text" name="location" placeholder="Location"  onChange={this.adjustInputValue} />
                    <input ref={input => this.genderInput = input} id="inputUserGender-React" type="text" name="gender" placeholder="Gender"  onChange={this.adjustInputValue}/>
                    <br/>
                    <button id="btnAddUser" onClick={this.submitUser}>Add User (React)</button>
                </fieldset>
            </div>
        )
    }
}