function UserInfo (props) {
  return(
      <div id="userInfoWrapper">
       <h2>User Info</h2>
       <div id="userInfoBox">
        <p><strong>Name: </strong>{props.focusedUser ? props.focusedUser.fullname : ''}</p>
        <p><strong>Age: </strong>{props.focusedUser ? props.focusedUser.age : '' }</p>
        <p><strong>Gender: </strong>{props.focusedUser ? props.focusedUser.gender : '' }</p>
        <p><strong>Location: </strong>{props.focusedUser ? props.focusedUser.location: '' }</p>
       </div>
      </div>
  )
}
