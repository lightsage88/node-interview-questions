class UserList extends React.Component {
    constructor(props){
        super(props)
        this.state={

        };
    }
  


    render(){
        return(
            

            <div id="userList-React">
            <h2>User List</h2>
                <table>
                    <thead>
                        <tr>
                            <th>UserName</th>
                            <th>Email</th>
                            <th>Delete?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* Users will go here after we generate a component from props */}
                        {this.props.userRows}
                    </tbody>
                </table>
            </div>
        )
    }
}