var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");

var url = 'http://localhost:3000';

describe('Add User', function(){
	it('Adds a new user with user name \'test user\'', function(done){
		var newUser = {
			'username' : 'test user',
			'email' : 'test1@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		//it might be good to say something like expect res.code to equal 200 instead of just expect 201
		//we want easy errors to be like low-hanging fruit in terms of difficulty
		.expect(200)
		.end(function(err, res){
			if(err){
				throw err;
			}
			done();
		});
	});
});