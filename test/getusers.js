var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Get Users', function(){

	before(()=>{
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.drop()
	})
	it('Gets all users', function(done){
		request(url)
		.get('/users/userlist')
		
		.expect(200)
		.end(function(err, res){
			if(err){
				throw err;
			}
			var users = JSON.parse(res.text);
			expect(users.length).toEqual(0);
			done();
		});
	});
});